import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryTrackingComponent } from './components/inventory-tracking/inventory-tracking.component';
import { SaleComponent } from './components/sale/sale.component';
import { InboundComponent } from './components/inbound/inbound.component';
import { SalesTrackingComponent } from './components/sales-tracking/sales-tracking.component';

const routes: Routes = [
  { path: '', redirectTo: '/inbound', pathMatch: 'full' },
  { path: 'sale', component: SaleComponent },
  { path: 'inbound', component: InboundComponent },
  { path: 'sales-track', component: SalesTrackingComponent },
  { path: 'inventory-track', component: InventoryTrackingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
