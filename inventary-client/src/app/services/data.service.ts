import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  updateTotalSource = new BehaviorSubject<string>('');
  ObserveUpdateTotal = this.updateTotalSource.asObservable();

  constructor(private http: HttpClient) { }

  addProduct(data: Object, cb?: Function): void {
    const url = `${environment.apiHost}/addProduct`;
    this.http.post(url, data).subscribe(res => {
      if (cb) { cb(null, res); }
    }, err => {
      console.log(err);
      if (cb) { cb(err); }
    });
  }
  getProducts(cb?: Function): void {
    const url = `${environment.apiHost}/getProducts`;
    this.http.get(url).subscribe(res => {
      if (cb) { cb(null, res); }
    }, err => {
      console.log(err);
      if (cb) { cb(err); }
    });
  }

  buyProducts(data: Object, cb?: Function): void {
    data['cdate'] = Math.round(Date.now() / 1000);
    console.log(data);
    const url = `${environment.apiHost}/addProductSales`;
    this.http.post(url, data).subscribe(res => {
      if (cb) { cb(null, res); }
    }, err => {
      console.log(err);
      if (cb) { cb(err); }
    });
  }

  getSales(cb?: Function): void {
    const url = `${environment.apiHost}/getProductSales`;
    this.http.get(url).subscribe(res => {
      if (cb) { cb(null, res); }
    }, err => {
      console.log(err);
      if (cb) { cb(err); }
    });
  }

  updateTotal(): void {
    this.updateTotalSource.next('');
  }
}
