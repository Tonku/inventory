import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SelectModule } from 'ng2-select';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { InboundComponent } from './components/inbound/inbound.component';
import { SaleComponent } from './components/sale/sale.component';
import { InventoryTrackingComponent } from './components/inventory-tracking/inventory-tracking.component';
import { SalesTrackingComponent } from './components/sales-tracking/sales-tracking.component';
import { DataService } from './services/data.service';
import { SaleItemComponent } from './components/sale-item/sale-item.component';

@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    InboundComponent,
    SaleComponent,
    InventoryTrackingComponent,
    SalesTrackingComponent,
    SaleItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    SelectModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
