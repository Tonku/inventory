import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-inventory-tracking',
  templateUrl: './inventory-tracking.component.html',
  styleUrls: ['./inventory-tracking.component.scss']
})
export class InventoryTrackingComponent implements OnInit {
  products =[];
  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.dataService.getProducts((err, response) => {
      this.products = response['data'];
    });
  }

}
