import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-inbound',
  templateUrl: './inbound.component.html',
  styleUrls: ['./inbound.component.scss']
})
export class InboundComponent implements OnInit {

  product = {};

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  save(): void {
    this.dataService.addProduct(this.product);
  }

}
