import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

declare var $: any;

@Component({
  selector: 'app-sales-tracking',
  templateUrl: './sales-tracking.component.html',
  styleUrls: ['./sales-tracking.component.scss']
})
export class SalesTrackingComponent implements OnInit {
  sales = [];
  selectedSales = [];
  startDate: number;
  endDate: number;
  constructor(private dataService: DataService) { }

  ngOnInit() {

    function cb(start, stop) {
      $('#drp').html(start.format('MMMM D, YYYY') + ' - ' + stop.format('MMMM D, YYYY'));
    }

    $('#drp').daterangepicker({}, cb);

    $('#drp').on('apply.daterangepicker', (ev, picker) => {

      this.startDate = (picker.startDate._d.setHours(0, 0, 0, 0)) / 1000;
      this.endDate = (picker.endDate._d.setHours(0, 0, 0, 0)) / 1000;

      this.selectedSales = this.sales.filter(item => item['cdate'] >= this.startDate && item['cdate'] <= this.endDate);

      console.log(this.startDate, this.endDate);
    });

    // , function (start, end, label) {
    //   this.startDate = start._d.getTime();

    //   this.endDate = end._d.getTime();
    //   console.log(this.startDate, this.endDate);
    //   // this.endDate = Math.round(end._d.getTime() / 1000);
    //   console.log();
    //   console.log(start, end);
    //   // console.log('A new date selection was made: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    // }

    this.dataService.getSales((err, response) => {
      if (err || !response) { return; }
      this.sales = response['data'];
      this.selectedSales = this.sales;
      console.log(this.sales);
    });
  }

}
