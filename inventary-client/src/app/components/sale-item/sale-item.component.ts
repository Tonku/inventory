import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-sale-item',
  templateUrl: './sale-item.component.html',
  styleUrls: ['./sale-item.component.scss']
})
export class SaleItemComponent implements OnInit {

  @Input() sale: Object;
  items = [];
  products: Array<Object>;

  // private productSearchTerm = new Subject<string>();

  private productSearchTerm = new Subject<string>();

  // , 'Mouse', 'Mi A1', 'Pen', 'Keyboard'


  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.getProducts((err, response) => {
      if (err || !response) { return; }
      this.products = response['data'];
      this.items = this.products.map(item => {
        return { id: item['_id'], text: item['name'] };
      });
    });
  }

  refreshProduct(x: any): void {
    console.log(x);
  }

  selected(x: any): void {
    const sp = this.products.find(e => e['_id'] === x.id);
    this.sale['psno'] = sp['sno'];
    this.sale['price'] = sp['ppu'];
    this.sale['pname'] = x['text'];
  }
  removed(x: any): void {
    console.log(x);
  }
  typed(x: string): void {
    this.productSearchTerm.next(x);
  }

  changeItemPrice(): void {
    this.sale['qty'] = this.sale['qty'] || 0;
    this.sale['iprice'] = this.sale['price'] * this.sale['qty'];
    this.dataService.updateTotal();
  }

  // search(term: string): void {
  //   this.productSearchTerm.next(term);
  // }
}
