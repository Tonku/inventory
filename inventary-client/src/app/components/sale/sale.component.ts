import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {
  sale = { products: [{}] };

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.ObserveUpdateTotal.subscribe(res => {
      this.sale['tprice'] = this.sale.products.reduce((a, b) => a + b['iprice'], 0) || 0;
    });
  }

  buy(): void {
    this.dataService.buyProducts(this.sale);
  }

  addAnotherProduct(): void {
    this.sale.products.push({});
  }

}
