from configurations import config as config_config

api_connection_result = config_config.get_api_connection_config()

api_types = {
    'which_type': api_connection_result['which_type']  # development staging production
}

local_server_configurations = {
    "api": {
        "host": "0.0.0.0",
        "port":1413,
        "debug": True
    }
}

prod_server_configurations = {
    "api": {
        "host": "prod_ip",
        "port": 1413,
        "debug":False
    }
}


def get_api_types():
    if api_types['which_type'] == 'production':
        return prod_server_configurations['api']
    else:
        return local_server_configurations['api']

