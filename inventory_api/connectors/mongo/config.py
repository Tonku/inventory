from pymongo import MongoClient
from configurations import config as config_config

mongo_connection_result = config_config.get_mongo_connection_config()

mongo_types = {
    'which_type': mongo_connection_result['which_type']  # development staging production
}

local_server_configurations = {
    "mongo": {
        "local_server_host": "0.0.0.0"
    }
}

prod_server_configurations = {
    "mongo": {
        "prod_server_host": "prod_ip",
        "prod_server_user": "prod_host",
        "prod_server_pass": "prod_pass"
    }
}


def get_mongo_connection(which_type):
    if which_type == "development":
        client = MongoClient(local_server_configurations["mongo"]["local_server_host"], 27017)
        db = client.inventory
        return {'db': db}
    if which_type == "production":
        client = MongoClient(prod_server_configurations["mongo"]["prod_server_host"], 27017)
        client.adwords.authenticate(prod_server_configurations["mongo"]["prod_server_user"],
                                    prod_server_configurations["mongo"]["prod_server_pass"])
        db = client.inventory
        return {'db': db}


def get_mongo_types():
    return mongo_types
