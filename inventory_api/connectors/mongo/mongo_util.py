from connectors.mongo import config
from pymongo import ReturnDocument

mongo_types = config.get_mongo_types()

data_base_connection = config.get_mongo_connection(mongo_types['which_type'])
db = data_base_connection['db']

field_counter_param = 'seq'


def get_next_seq_value(collection_name):
    update_obj = {"$inc": {field_counter_param: 1}}
    result = db.counters.find_one_and_update({"_id": collection_name}, update_obj, upsert=True,
                                             return_document=ReturnDocument.AFTER)
    return result
