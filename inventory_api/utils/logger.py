import logging
import os


class Logger:
    logger = None

    def __init__(self, filename):
        self.logger = logging.getLogger(filename)
        handler = logging.StreamHandler()
        formatter = logging.Formatter(
            '%(asctime)s %(name)-12s line:%(lineno)-4s %(levelname)-8s %(message)-5s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        if 'LOG_LEVEL' in os.environ and int(os.environ['LOG_LEVEL']) in \
           [10, 20, 30, 40, 50]:
            level = int(os.environ['LOG_LEVEL'])
        else:
            level = logging.INFO
        self.logger.setLevel(level)

    def get_logger(self):
        return self.logger
