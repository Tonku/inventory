from flask import (
    Blueprint, request
)
from connectors.mongo import mongo_util
from utils.logger import Logger

bp = Blueprint('inventory', __name__)
logger = Logger(__name__).get_logger()


db = mongo_util.db


@bp.route('/addProduct',methods=['POST'])
def addProduct():
    try:
        data = request.data
        obj = {}
        if 'name' in data and data['name']:
            obj['name'] = data['name']
        else:
            return {'data': {'error': 'Bad Request','message':'Product Name invalid/Empty'}, 'status': 400}
        if 'sno' in data and data['sno']:
            obj['sno'] = data['sno']
        else:
            return {'data': {'error': 'Bad Request','message':'Product Serial Number invalid/Empty'}, 'status': 400}
        if 'qty' in data and data['qty'] and isinstance(data['qty'],int):
            obj['qty'] = data['qty']
            obj['available'] = data['qty']
        else:
            return {'data': {'error': 'Bad Request','message':'Quantities Received invalid/Empty'}, 'status': 400}
        if 'ppu' in data and data['ppu'] and isinstance(data['ppu'],int):
            obj['ppu'] = data   ['ppu']
        else:
            return {'data': {'error': 'Bad Request','message':'Price per unit invalid/Empty'}, 'status': 400}
        if 'vname' in data and data['vname']:
            obj['vname'] = data['vname']
        else:
            return {'data': {'error': 'Bad Request','message':'Vendor Name invalid/Empty'}, 'status': 400}
        result = mongo_util.get_next_seq_value('inbound')
        try:
            pId = result['seq']
            obj['_id'] = pId
            db.inbound.insert(obj)
            return {'data': {'id': pId}, 'status': 200}
        except Exception as e:
            logger.error(str(e))
            return {'status': 500, "error": "Something went wrong."}
    except Exception as e:
        logger.error(str(e))
        return {'status': 500, "error": "Something went wrong."}

@bp.route('/getProducts', methods=['GET'])
def getProducts():
    try:
        data = list(db.inbound.find({'available':{'$gt':0}}))
        return {'data':data,'status':200}
    except Exception as e:
        logger.error(str(e))
        return {'data': {"error": "Something went wrong."}, 'status': 500}

@bp.route('/getProductByName', methods=['GET'])
def getProductByName():
    try:
        name = request.args.get('name', None)
        if name:
            data = db.inbound.find_one({'name': name})
            if data:
                return {'data':data,'status':200}
            else:
                return {'data': {"error":"Not Found"}, 'status': 201}
        else:
            return {'data': {'error': 'Bad Request'}, 'status': 400}
        # data = list(db.inbound.find({'available':{'$gt':0}}))
    except Exception as e:
        logger.error(str(e))
        return {'data': {"error": "Something went wrong."}, 'status': 500}

@bp.route('/addProductSales',methods=['POST'])
def addProductSales():
    try:
        data = request.data
        obj = {}
        if 'cname' in data and data['cname']:
            obj['cname'] = data['cname']
        else:
            return {'data': {'error': 'Bad Request', 'message': 'Customer Name invalid/Empty'}, 'status': 400}
        if 'caddr' in data and data['caddr']:
            obj['caddr'] = data['caddr']
        else:
            return {'data': {'error': 'Bad Request', 'message': 'Customer Address invalid/Empty'}, 'status': 400}
        if 'cdate' in data and data['cdate']:
            obj['cdate'] = data['cdate']
        else:
            return {'data': {'error': 'Bad Request', 'message': 'Current Date invalid/Empty'}, 'status': 400}
        if 'products' in data and data['products']:
            obj['products'] = data['products']
        else:
            return {'data': {'error': 'Bad Request', 'message': 'Products purchased invalid/Empty'}, 'status': 400}
        if 'tprice' in data and data['tprice']:
            obj['tprice'] = data['tprice']
        else:
            return {'data': {'error': 'Bad Request', 'message': 'Total Price invalid/Empty'}, 'status': 400}
        try:
            result = mongo_util.get_next_seq_value('sales')['seq']
            obj['_id'] = result
            db.sales.insert(obj)
            for product in obj['products']:
                db.inbound.update({'name':product['pname'],'sno':product['psno']},{'$inc':{'available': -1*product['qty']}})
            return {'data':{'id':result},'status':200}
        except Exception as e:
            logger.error(str(e))
            return {'data': {"error": "Bad Request."}, 'status': 400}
    except Exception as e:
        logger.error(str(e))
        return {'data': {"error": "Something went wrong."}, 'status': 500}

@bp.route('/getProductSales', methods=['GET'])
def getProductSales():
    try:
        data = list(db.sales.find({}))
        return {'data':data,'status':200}
    except Exception as e:
        logger.error(str(e))
        return {'data': {"error": "Something went wrong."}, 'status': 500}