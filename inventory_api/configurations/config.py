mongo_server_type = "development"  # development production
api_server_type = "development" # development production

def get_mongo_connection_config():
    return {
        'which_type': mongo_server_type
    }


def get_api_connection_config():
    return {
        'which_type': api_server_type
    }