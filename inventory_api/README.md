# adwords-api
### Virtual environment
```sh
$ mkvirtualenv -p /usr/bin/python2.7 inventory
```
### Installation
```sh
$ pip install -r requirements.txt
```
### Main
basic_api.py

```sh
$ python api.py
```

## Configuration To DB

change `IP` in connectors/mongo/config to required `IP`