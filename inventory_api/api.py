from authentication.auth import requires_auth
from flask_api import FlaskAPI
import inventory
from connectors.api import api

app = FlaskAPI(__name__)

app.register_blueprint(inventory.bp)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
#@requires_auth
def home(path):
    return 'Hello, Welcome to ADWORDS Admin api ' + path


api_cred = api.get_api_types()
if __name__ == '__main__':
    app.run(debug=api_cred['debug'], host=api_cred['host'], port=api_cred['port'])
